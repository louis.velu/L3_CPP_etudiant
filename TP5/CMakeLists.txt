cmake_minimum_required( VERSION 3.0 )
project( TP5 )
set( CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++14 -Wall -Wextra" )

find_package( PkgConfig REQUIRED )
pkg_check_modules( PKG_CPPUTEST REQUIRED cpputest )
pkg_check_modules( GTKMM gtkmm-3.0)
include_directories( ${PKG_CPPUTEST_INCLUDE_DIRS} )

# programme principal
link_directories(${GTKMM_LIBRARY_DIRS})
include_directories(${GTKMM_INCLUDE_DIRS})
add_executable( main.out src/main.cpp src/FigureGeometrique.cpp src/Ligne.cpp src/PolygoneRegulier.cpp src/ViewerFigures.cpp src/ZoneDessin.cpp)
target_link_libraries( main.out ${GTKMM_LIBRARIES})

# programme de test
#add_executable( main_test.out src/main_test.cpp src/Ligne.cpp src/Ligne_test.cpp src/PolygoneRegulier.cpp src/PolygoneRegulier_test.cpp)
#target_link_libraries( main_test.out ${PKG_CPPUTEST_LIBRARIES} )
