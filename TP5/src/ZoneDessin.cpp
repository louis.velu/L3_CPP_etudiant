//ZoneDessin.cpp
//Louis Vélu
#include "ZoneDessin.hpp"
#include <gtkmm.h>
#include <vector>
#include "PolygoneRegulier.hpp"
#include "Couleur.hpp"
#include "Point.hpp"
#include <iostream>

ZoneDessin::ZoneDessin(){
	//on connecte signal_button_press_event a generClic
	signal_button_press_event().connect(&ZoneDessin::gererClic(GdkEventButton*));


	//ajouter quelques figures geometrique a _figures
	Couleur couleur;
	couleur._r=1;
	couleur._g=0;
	couleur._b=0;

	Point centre;	
	centre._x=100;
	centre._y=100;


	_figures.push_back(new PolygoneRegulier(couleur, centre, 100, 3));
}

ZoneDessin::~ZoneDessin(){//suprimer les figures
	for (unsigned i =0; i<_figures.size(); i++){
		delete _figures.at(i);
	}
}

bool ZoneDessin::on_draw(const Cairo::RefPtr<Cairo::Context> & context){
	for (unsigned i =0; i<_figures.size(); i++){
		_figures.at(i)->afficher(context);
	}
	return true;
}

bool ZoneDessin::gererClic(GdkEventButton* event){
	if(event->button == 1){//clic gauche
		Couleur couleur;
		couleur._r=1;
		couleur._g=0;
		couleur._b=0;
		Point centre;	
		centre._x=event->x;
		centre._y=event->y;
		_figures.push_back(new PolygoneRegulier(couleur, centre, 100, 3));
	}else if (event->button == 3){ //clic droit
		if(_figures.size() > 0){//si il reste des figures
			delete _figures.at(_figures.size()-1);
			_figures.pop_back();
		}
	}
	get_window()->invalidate(true);
	return true;
}
