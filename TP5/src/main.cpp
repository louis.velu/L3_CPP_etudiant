//main.cpp
//Louis Vélu

#include <iostream>
#include "Point.hpp"
#include "Couleur.hpp"
#include "FigureGeometrique.hpp"
#include "Ligne.hpp"
#include "PolygoneRegulier.hpp"
#include <vector>
#include <gtkmm.h>
#include "ViewerFigures.hpp"
#include "ZoneDessin.hpp"

int main(int argc, char** argv){
	/** TEST **/
	/*
	Point p0, p1;
	Couleur c;
	p0._x = p0._y = 0;
	p1._x = 100;
	p1._y = 200;
	c._r = 1.0;
	c._g = c._b = 0;

	std::vector<FigureGeometrique> vec;

	Ligne l1(c, p0, p1);
	vec.push_back(l1);

	PolygoneRegulier pol_reg(c, p1, 50, 5);
	vec.push_back(pol_reg);
	
	for(unsigned int i=0; i<vec.size(); i++){
		vec.at(i).afficher();
	}
	*/
	

	/** GTK **/
	/* sans orienté objet */
	/*
	Gtk::Main kit(argc, argv);//app gtkmm
	Gtk::Window window;//creation de la fenêtre
	window.set_title("SuperViewer 1.0");
	//add fig
	window.show_all();//on affiche le fenêtre
	kit.run(window);//lance la boucle d'évent
	*/
	/* orienté objet */
	ViewerFigures view(argc, argv);
	view.run();

	return 0;
}
