//PolygoneRegulier.cpp
//Louis Vélu

#include "PolygoneRegulier.hpp"
#include "FigureGeometrique.hpp"
#include "Point.hpp"
#include "Couleur.hpp"
#include <iostream>
#include <math.h>
#include <string>
#include <gtkmm.h>

PolygoneRegulier::PolygoneRegulier(const Couleur& couleur, const Point& centre, int rayon, int nbCotes) : FigureGeometrique(couleur) {
	if(nbCotes < 3 ){
		throw std::string("Un polynome régulier doit avoir plus de 3 cotes");	
	}
	_nbPoints = nbCotes;
	_points = new Point[_nbPoints];
	for (int i=0; i<_nbPoints; i++){
		double angle = i*2*M_PI/_nbPoints;
		Point p;
		p._x = (int)((double)centre._x + cos(angle)*rayon);
		p._y = (int)((double)centre._y + sin(angle)*rayon);
		_points[i] = p;
	}
	

	
	
}
void PolygoneRegulier::afficher(const Cairo::RefPtr<Cairo::Context> & context)const{
	context->set_source_rgb(_couleur._r, _couleur._g, _couleur._b);
	context->set_line_width(2.0);
	context->move_to(_points[0]._x,_points[0]._y);
	for(int i=1; i<_nbPoints; i++){
		context->line_to(_points[i]._x,_points[i]._y);
	}
	context->line_to(_points[0]._x,_points[0]._y);
	context->stroke();




	/*
	std::cout << "PolygoneRegulier" << " " << _couleur._r << "_" << _couleur._g << "_" << _couleur._b;
	for (int i=0; i<_nbPoints; i++){
		std::cout << " " << _points[i]._x << "_" << _points[i]._y;
	}
	std::cout << std::endl;
	*/
}
int PolygoneRegulier::getNbPoints(){
	return _nbPoints;
}
const Point& PolygoneRegulier::getPoint(int indice)const{
	if(indice < 0 || indice > _nbPoints){
		throw std::string("indice incorrecte");
	}
	return _points[indice];
}

PolygoneRegulier::~PolygoneRegulier(){
	delete [] _points;
}	
