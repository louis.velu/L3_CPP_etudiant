//Image.hpp
//Louis Vélu
#ifndef IMAGE_HPP_
#define IMAGE_HPP_
	#include <string>
	class Image{
		private:
			int _largeur;
			int _hauteur;
			int* _pixels;

		public:
			Image(int largeur, int hauteur);
			~Image();
			int getLargeur()const;
			int getHauteur()const;
			/*int getPixel(int i, int j)const;
			void setPixel(int i, int j, int couleur);*/
			const int& getPixel(int i, int j)const;
			void setPixel(int i, int j, const int& couleur);
	};
	void ecrirePnm(const Image& img, const std::string& monFichier);
	void remplir(Image & img);
	Image bordure(const Image& img, int couleur, int epaisseur);
	Image& operator=(const Image& img);
#endif
