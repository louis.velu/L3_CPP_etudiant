//Image.cpp
//Louis Vélu

#include "Image.hpp"
#include <fstream>
#include <iostream>
#include <string>
#include <sstream>
#include <cmath>
Image::Image(int largeur, int hauteur): _largeur(largeur), _hauteur(hauteur){
	_pixels = new int[_largeur*_hauteur]();
}

Image::~Image(){
	delete [] _pixels;
}

int Image::getLargeur()const{
	return _largeur;
}
int Image::getHauteur()const{
	return _hauteur;
}
/*
int Image::getPixel(int i, int j)const{
	return _pixels[i*_largeur+j];
}
void Image::setPixel(int i, int j, int couleur){
	_pixels[i*_largeur+j] = couleur;
}
*/


const int& Image::getPixel(int i, int j)const{
	return _pixels[i*_largeur+j];
}
void Image::setPixel(int i, int j, const int& couleur){
	_pixels[i*_largeur+j] = couleur;
}

void ecrirePnm(Image& img, const std::string& monFichier){
	//on ouvre le fichier
	std::fstream file;
	file.open(monFichier, std::ios::in);
	if(!file.is_open()){
		throw std::string("fichier PNM introuvable");
	}
	//on verifie qu'il est au bon type
	std::string test;
	file >> test;
	if(test != "P1" || test != "P2"){
		throw std::string("le fichier n'est pas au type P1 ou P2");
	}
	//on lit les dimension	
	std::stringstream reader;
	std::string tmp;
	file >> tmp;
	reader << tmp;	
	int largeur;
	int hauteur;
	reader >> largeur;
	reader >> hauteur;
	img = Image(largeur, hauteur);
	
	for(int i=0; i < hauteur; i++){
		if(file.eof()){
			throw std::string("erreur dans le format du fichier");
		}
		file >> tmp;
		reader << tmp;	
		for(int e=0; e<largeur; e++){
			if(reader.eof()){
				throw std::string("erreur dans le format du fichier");
			}
			int c;
			reader >> c;
			img.setPixel(i, e, c);	
		}
	}
	
}

void remplir(Image & img){
	img = Image(100,100);
	for(int i=0; i<100; i++){
		int c = (int)((std::cos((float)i/20)+1)*7);
		for(int e=0; e<100; e++){
			img.setPixel(e,i,c);
		}
	}

}


Image bordure(const Image& img, int couleur, int epaisseur){
	if(epaisseur > img.getLargeur() || epaisseur > img.getHauteur()){
		throw std::string("epaisseur de la brodure trop grand");
	}
	Image img2 = img;
		
}

const Image& operator=(const Image& img){
	Image img2 = Image(img.getLargeur(), img.getHauteur());
	for(int i=0; i < img.getHauteur(); i++){
		for(int e=0; e<img.getLargeur(); e++){
			img2.setPixel(i,e,img.getPixel(i,e));
		}
	}
}
