// Fibonacci_test.cpp
// louis vélu
#include "Vecteur3.hpp"
#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupVec3) { };

TEST(GroupVec3, test_norm){
    Vecteur3 vec {2,3,6};
    CHECK_EQUAL(vec.norme(), 7);

}
