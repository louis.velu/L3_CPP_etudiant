//Vecteur3.cpp
//louis vélu
#include "Vecteur3.hpp"
#include <iostream>
#include <math.h>

void afficher(const Vecteur3 &vec){
    std::cout << "(" << vec.x << "," << vec.y << "," << vec.z << ")" << std::endl;
}
void Vecteur3::afficher() const {
    std::cout << "(" << x << "," << y << "," << z << ")" << std::endl;
}


float Vecteur3::norme()const{
    return sqrt(x*x+y*y+z*z);
}
float Vecteur3::produitScalaire(Vecteur3 vec2)const{
    return x*vec2.x+y*vec2.y+z*vec2.z;
}
Vecteur3 Vecteur3::addition(Vecteur3 vec2)const{
    return Vecteur3 {x+vec2.x,y+vec2.y,z+vec2.z};
}
