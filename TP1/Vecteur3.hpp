// Vecteur3.hpp
//louis vélu
#ifndef _Vecteur3_hpp
#define _Vecteur3_hpp
struct Vecteur3 {
    float x,y,z;
    void afficher()const;
    float norme()const;
    float produitScalaire(Vecteur3 vec2)const;
    Vecteur3 addition(Vecteur3 vec2)const;
};

void afficher(const Vecteur3 &vec);

#endif
