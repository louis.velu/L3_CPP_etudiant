/*Louis Vélu*/
#include "Fibonacci.hpp"
/*on suppose n>0*/
int fibonacciRecursif(int n){
	if(n==1)return 1;
	if(n==2)return 1;
	return fibonacciRecursif(n-1)+fibonacciRecursif(n-2);
}

/*on suppose n>0*/
int fibonnaciIteratif(int n){
	if(n==1)return 1;
	if(n==2)return 1;	
	int res=0;
	int n1=1;
	int n2=1;
	for(int i=2; i<n; i++){
		res=n1+n2;
		n1=n2;
		n2=res;
	}
	return res;
}
