// Fibonacci_test.cpp
// louis vélu
#include "Fibonacci.hpp"
#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupFibonacci) { };

TEST(GroupFibonacci, test_fibonacciRecursif){
    int n1 = fibonacciRecursif(1);
    int n2 = fibonacciRecursif(2);
    int n3 = fibonacciRecursif(3);
    int n4 = fibonacciRecursif(4);
    int n5 = fibonacciRecursif(5);
    CHECK_EQUAL(1, n1);
    CHECK_EQUAL(1, n2);
    CHECK_EQUAL(2, n3);
    CHECK_EQUAL(3, n4);
    CHECK_EQUAL(5, n5);

}

TEST(GroupFibonacci, test_fibonnaciIteratif){
    int n1 = fibonnaciIteratif(1);
    int n2 = fibonnaciIteratif(2);
    int n3 = fibonnaciIteratif(3);
    int n4 = fibonnaciIteratif(4);
    int n5 = fibonnaciIteratif(5);
    CHECK_EQUAL(1, n1);
    CHECK_EQUAL(1, n2);
    CHECK_EQUAL(2, n3);
    CHECK_EQUAL(3, n4);
    CHECK_EQUAL(5, n5);

}
