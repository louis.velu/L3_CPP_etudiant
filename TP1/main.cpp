/*Louis Vélu*/
#include <iostream>
#include "Fibonacci.hpp"
#include "Vecteur3.hpp"

int main(){
	std::cout<<"Recursif:"<<fibonacciRecursif(7) << std::endl;
	std::cout<<"Iteratif:"<<fibonnaciIteratif(7) << std::endl;
    Vecteur3 vec3 {2,3,6};
    Vecteur3 vec32 {6,3,2};
   // afficher(vec3);
    vec3.afficher();
    std::cout << vec3.norme() << std::endl;
    std::cout << vec3.produitScalaire(vec32) << std::endl;
    Vecteur3 tmp = vec3.addition(vec32);
    tmp.afficher();
	return 0;
}
