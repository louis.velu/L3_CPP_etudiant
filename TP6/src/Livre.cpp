//Louis Vélu
//Livre.cpp
#include "Livre.hpp"
#include <string>
#include <vector>
#include <iostream>

Livre::Livre(){
	/*rien*/
}
Livre::Livre(const std::string & titre, const std::string & auteur, int annee) : _titre(titre), _auteur(auteur), _annee(annee) {
	if(_auteur.find("\n") != std::string::npos){
		throw std::string("erreur : auteur non valide ('\n' non autorisé)");
	}
	if(_auteur.find(";") != std::string::npos){
		throw std::string("erreur : auteur non valide (';' non autorisé)");
	}
	if(_titre.find("\n") != std::string::npos){
		throw std::string("erreur : titre non valide ('\n' non autorisé)");
	}

	if(_titre.find(";") != std::string::npos){
		throw std::string("erreur : titre non valide (';' non autorisé)");
	}
}
const std::string & Livre::getTitre()const{
	return _titre;
}
const std::string & Livre::getAuteur()const{
	return _auteur;
}
int Livre::getAnnee()const{
	return _annee;
}

bool Livre::operator<(const Livre & l2)const{
	if(_auteur < l2._auteur ){
		return true;
	}else if(_auteur == l2.getAuteur()){
		return _titre < l2._titre;
	}
	return false;
	
}

bool  Livre::operator==(const Livre & l2)const{
	return _auteur == l2._auteur && _titre == l2._titre && _annee == l2._annee;
}


std::ostream& operator<<(std::ostream& os, const Livre& l1){
	os << l1.getTitre() << ";" << l1.getAuteur() << ";" << l1.getAnnee();
	return os;
}

std::istream& operator>>(std::istream& is, Livre& l1){
	std::string input;
	is >> input;
	l1._titre = input.substr(0,input.find(';'));
	input=input.substr(input.find(';')+1,input.length()-1);
	l1._auteur = input.substr(0,input.find(';'));
	l1._annee = atoi(input.substr(input.find(';')+1,input.length()-1).c_str());
	return is;
}









