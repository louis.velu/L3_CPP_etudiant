//FigureGeometrique.hpp
//Louis Vélu
#ifndef FIGUREGEOMETRIQUE_HPP_
#define FIGUREGEOMETRIQUE_HPP_
	#include "Couleur.hpp"

	class FigureGeometrique{
		protected: //accès dans le class et dans les class filles
			Couleur _couleur;	
	
		public: //accès pour tout le monde
			FigureGeometrique(const Couleur& couleur);
			const Couleur& getCouleur() const;
			virtual void afficher()const;
	};

#endif
