//PolygoneRegulier.cpp
//Louis Vélu

#include "PolygoneRegulier.hpp"
#include "FigureGeometrique.hpp"
#include "Point.hpp"
#include "Couleur.hpp"
#include <iostream>
#include <math.h>
#include <string>

PolygoneRegulier::PolygoneRegulier(const Couleur& couleur, const Point& centre, int rayon, int nbCotes) : FigureGeometrique(couleur) {
	if(nbCotes < 3 ){
		throw std::string("Un polynome régulier doit avoir plus de 3 cotes");	
	}
	_nbPoints = nbCotes;
	_points = new Point[_nbPoints];
	for (int i=0; i<_nbPoints; i++){
		double angle = i*2*M_PI/_nbPoints;
		Point p;
		p._x = (int)((double)centre._x + cos(angle)*rayon);
		p._y = (int)((double)centre._y + sin(angle)*rayon);
		_points[i] = p;
	}
	

	
	
}
void PolygoneRegulier::afficher()const{
	std::cout << "PolygoneRegulier" << " " << _couleur._r << "_" << _couleur._g << "_" << _couleur._b;
	for (int i=0; i<_nbPoints; i++){
		std::cout << " " << _points[i]._x << "_" << _points[i]._y;
	}
	std::cout << std::endl;
}
int PolygoneRegulier::getNbPoints(){
	return _nbPoints;
}
const Point& PolygoneRegulier::getPoint(int indice)const{
	if(indice < 0 || indice > _nbPoints){
		throw std::string("indice incorrecte");
	}
	return _points[indice];
}

PolygoneRegulier::~PolygoneRegulier(){
	delete [] _points;
}	
