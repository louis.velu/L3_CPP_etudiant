#include "Ligne.hpp"

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupLigne) { };

TEST(GroupLigne, getP0)  {
	Point p0, p1;
	Couleur c;
	p1._x = p1._y = 0;
	p0._x = 100;
	p0._y = 200;
	c._r = 1.0;
	c._g = c._b = 0;

	Ligne l1(c, p0, p1);
    CHECK_EQUAL(l1.getP0(), p0);
}

TEST(GroupLigne, getP1)  {
	Point p0, p1;
	Couleur c;
	p0._x = p0._y = 0;
	p1._x = 100;
	p1._y = 200;
	c._r = 1.0;
	c._g = c._b = 0;

	Ligne l1(c, p0, p1);
    CHECK_EQUAL(l1.getP1(), p1);
}

