#include "Ligne.hpp"

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupPolygoneRegulier) { };

TEST(GroupPolygoneRegulier, getNbPoints)  {
	Point p0, p1;
	Couleur c;
	p0._x = p0._y = 0;
	p1._x = 100;
	p1._y = 200;
	c._r = 1.0;
	c._g = c._b = 0;

	PolygoneRegulier pol_reg(c, p1, 50, 5);

    CHECK_EQUAL(pol_reg.getNbPoints(), 5);
}

TEST(GroupPolygoneRegulier, getPoint)  {
	Point p0, p1, p3;
	Couleur c;
	p0._x = p0._y = 0;
	p1._x = 100;
	p1._y = 200;
	c._r = 1.0;
	c._g = c._b = 0;

	PolygoneRegulier pol_reg(c, p1, 50, 5);
	p3._x = 59;
	p3._y = 229;
    CHECK_EQUAL(pol_reg.getPoint(2), p3);
}

