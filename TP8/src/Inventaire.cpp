#include "Inventaire.hpp"
#include <sstream>
#include <vector>
#include <string>

/// Flux de sortie au format "<nom>;<date>;<volume>\n".
std::istream & operator>>(std::istream & is, Inventaire & i){
	std::locale vieuxLoc = std::locale::global(std::locale("fr_FR.UTF-8"));
	Bouteille b;
	while(is >> b){
		i._bouteilles.push_back(b);
	}
	std::locale::global(vieuxLoc);
	return is;
}

/// Flux d'entrée au format "<nom>;<date>;<volume>\n".
std::ostream & operator<<(std::ostream & os, const Inventaire & i){
	std::locale vieuxLoc = std::locale::global(std::locale("fr_FR.UTF-8"));
	for (auto & b : i._bouteilles){	
		os << b;
	}
	std::locale::global(vieuxLoc);
	return os;
}

