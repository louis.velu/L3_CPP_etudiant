#include "Controleur.hpp"

#include <cassert>
#include <fstream>
#include <iostream>
#include <sstream>

Controleur::Controleur(int argc, char ** argv) {
	_vues.push_back(std::make_unique<VueConsole>(*this));
   	_vues.push_back(std::make_unique<VueGraphique>(argc, argv, *this));
	//chargerInventaire("mesBouteilles.txt");
    for (auto & v : _vues)
      v->actualiser();
}

void Controleur::run() {
    for (auto & v : _vues)
        v->run();
}

std::stringstream Controleur::getTexte(){
	std::stringstream buffer;
	buffer << _inventaire;
	return buffer;
}


void Controleur::chargerInventaire(std::string nom_fichier){
	std::fstream file;
	file.open(nom_fichier, std::ios::in);
	if(file.is_open()){
		file >> _inventaire;
	}
	for (auto & v : _vues)
	      	v->actualiser();
}

