#ifndef INVENTAIRE_HPP_
#define INVENTAIRE_HPP_

#include "Bouteille.hpp"

#include <iostream>
#include <vector>

// Modèle : inventaire de bouteilles.
struct Inventaire {
    std::vector<Bouteille> _bouteilles;
};



/// Flux de sortie au format "<nom>;<date>;<volume>\n".
std::istream & operator>>(std::istream & is, Inventaire & i);

/// Flux d'entrée au format "<nom>;<date>;<volume>\n".
std::ostream & operator<<(std::ostream & os, const Inventaire & i);



#endif
